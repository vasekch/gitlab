---
redirect_to: '../shared-secrets.md'
---

This document was moved to [another location](../shared-secrets.md).

<!-- This redirect file can be deleted after <2021-06-22>. -->
<!-- Before deletion, see: https://docs.gitlab.com/ee/development/documentation/#move-or-rename-a-page -->
